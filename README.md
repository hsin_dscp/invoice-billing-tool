# invoice-billing-generator

## README.md

This repository contains code for performing various operations related to Azure Kubernetes Service (AKS) namespaces using GitLab CI/CD pipeline.

### Pipeline Stages

The `.gitlab-ci.yml` file defines the following stages in the pipeline:

1. **ArtifactFolderStructure**:
   - Creates the required folder structure for the InvoiceGenerator artifact.
   - Directories created: `InvoiceGenerator/InvoiceCSV`, `InvoiceGenerator/CompileData`, `InvoiceGenerator/RawData`, and `InvoiceGenerator/Log`.
   - The `InvoiceGenerator/` folder is included as an artifact.

2. **Prerequisite**:
   - Installs prerequisite packages required for the pipeline.
   - Uses the `python:3.9` Docker image.
   - Updates `pip` to the latest version.
   - Checks if the `InvoiceGenerator/site-packages/` directory already exists.
     - If it exists, displays a message indicating that the artifact is already present.
     - If it doesn't exist, downloads the artifact using a secure file URL.
   - Installs the following packages using `pip`:
     - `kubernetes==26.1.0`
     - `azure-identity==1.13.0`
     - `azure-mgmt-containerservice==24.0.0`
   - Copies the required files from `/usr/local/lib/python3.9/site-packages/` and `.secure_files/` to `InvoiceGenerator/`.
   - The `InvoiceGenerator/` folder is included as an artifact.

3. **AKSNamespaces**:
   - Retrieves and compiles AKS namespaces using the `AKSOperations.py` script.
   - Uses the `python:3.9` Docker image.
   - Copies the `InvoiceGenerator/site-packages/` directory to `/usr/local/lib/python3.9/`.
   - Lists the content of `/usr/local/lib/python3.9/site-packages/`.
   - Executes the `AKSOperations.py` script, which performs the following actions:
     - Reads the JSON config file from the secured file location.
     - Gets the AKS client object.
     - Retrieves all clusters in the resource group.
     - Extracts all namespaces and labels for each cluster.
     - Writes the raw namespace JSON file and compiled namespaces data to respective files in the `InvoiceGenerator/RawData/` and `InvoiceGenerator/CompileData/` directories.
   - Removes the `InvoiceGenerator/site-packages/` and `InvoiceGenerator/.secure_files` directories.
   - The `InvoiceGenerator/` folder is included as an artifact.

4. **ARIAToNamespaces**:
   - Retrieves and compiles hourly quota usage details for namespaces using the `AriaToOperations.py` script.
   - Uses the `python:3.9` Docker image.
   - Checks and installs below required Python packages using pip:
      1. requests 
      2. python-dateutil
   - Executes the `AriaToOperations.py` script, which performs the following actions:
     - Reads the billabe namepsace JSON file from artifacts.
     - Invokes Aria TO API to get hourly quota usage details for each namepsace.
     - Writes the raw API response JSON, detailed hourly quota usage JSON, compiled monthly quota usage along with billing calculations JSON as well as CSV files to `InvoiceGenerator/RawData/`, `InvoiceGenerator/CompileData/` and `InvoiceGenerator/InvoiceCSV/` directories in artifacts.

## Code Files

The repository includes the following code files:

  ### 1. AKSOperations.py:
      - Contains functions for retrieving AKS namespace details using the Azure SDK and Kubernetes Python client.
      - Reads the JSON config file from the secured file location.
      - Retrieves AKS client object using service principal credentials.
      - Retrieves and compiles AKS namespaces based on billing flags.
      - Writes the raw namespace JSON file and compiled namespaces data to respective files.
      - Logs error messages to `InvoiceGenerator/Log/trace.log` using a custom logger.
      - The main entry point is the `main()` function.
        ### Dependencies

        The script requires the following Python libraries to be installed:

        - `azure.identity` (version 1.13.0)
        - `azure.mgmt.containerservice` (version 24.0.0)
        - `kubernetes` (version 26.1.0)

        ** Main Functionality **

        1. Imports necessary modules and libraries.
        2. Obtains an AKS client object by calling the `GetAKSClient` function.
        2. Reads a JSON configuration file (`pipeline_configuration.json`) from a secured file location.
        3. Iterates over each cluster defined in the configuration file and calls the `ExtractAllNamespacesAndLabels` function for each cluster.
        4. The `ExtractAllNamespacesAndLabels` function performs the following steps for each cluster:
          - Retrieves the access profile for the cluster.
          - Extracts the kubeconfig file content from the access profile.
          - Creates a temporary kubeconfig file and loads the kubeconfig.
          - Deletes the temporary kubeconfig file after loading the configuration.
          - Creates an API client for interacting with the cluster.
          - Retrieves the list of namespaces for the cluster.
          - Retrieves deleted namespace list from configurations.
          - Compiles the namespaces and labels based on certain conditions.
          - Writes the compiled data to JSON files.

  ### 2. AriaToOperations.py
        - Reads billable namespace JSOn created by AKSOperations.py from artifacts.
        - Invokes Aria TO API to get hourly quota usage data for each of the namespace.
        - If data is received then generates the following files: 
          1. Raw JSON data files in the specified directory.
          2. Detailed JSON data files for each namespace containing hourly data.
          3. CSV files for each namespace containing hourly data.
          4. Aggregated JSON files containing monthly data.
          5. Aggregated CSV files containing monthly data.
          6. Contract-wise aggregated JSON and CSV files.

  ### 3. exception_handling.py

      This file provides exception handling functionality for the project. It sets up a logger to record error messages in the "Error.log" file located in the "InvoiceGenerator/Log" directory. The logger is configured to log messages with the following format: `<timestamp> <log level>: <log message>`.

      The file contains a decorator function named `log_exception`, which can be used to wrap other functions and handle any exceptions that occur within them. When an exception is raised, the decorator captures the exception information, including the type, message, and stack trace. It then logs this information using the configured logger and exits the program with an exit code of 1.

  ### 4. helper.py

    The `helper.py` file contains various helper functions used in the project. These functions are as follows:

    - `ReadJSONFile(Filelocation)`: This function reads a JSON file from the specified location and returns its contents as a Python dictionary.
    - `ReadJSONFileFromCurrentDirectory(Filelocation)`: This function reads a JSON file from the current directory and returns its contents as a Python dictionary. It constructs the file path by combining the current directory path with the provided file location.
    - `getConfigVarValue(configKey)`: This function retrieves the value of a configuration variable based on the environment. If the environment is set to 'local', the function returns the `configKey` parameter as is. Otherwise, it retrieves the value from the corresponding environment variable.
    - `convertJSONToCsvAndAppendToFile(JSONdata, CsvFilePath)`: This function converts JSON data into the CSV file format and writes it to the specified location. It expects a list of dictionaries as input, where each dictionary represents a row in the resulting CSV file. The function uses the keys of the first dictionary as the CSV header.
    - `ReadArtifactsFile` : This function reads an artifacts file from the specified artifacts folder location.
    - `convert_epoch_millis_to_datetime` : Converts epoch milliseconds to a datetime object.
    - `convertUTCDateToJST` : Converts a UTC datetime object to Japan Standard Time (JST).
    - `get_start_end_dates` : Calculates start and end dates based on a month offset. 
    - `calculate_sum_by_hour` : Processes JSON data to calculate resource quota utilization on an hourly basis.
    - `perform_monthly_calculation` : Performs monthly aggregation of resource quota utilization data.
    - `calcuate_by_contract_id` : function aggregates data based on contract IDs and unit prices.

  ### 5. pipeline_configuration.json

    The `pipeline_configuration.json` file represents the input data for

    the project. It follows the following structure:

    ```json
    {	
      "AZURE_AKS_LIST":[
        {
          "AKS_NAME":"<AKS_RUN_CLUSTER_NAME>"
        }
      ],
      "VALUE_OF_BILLING":
      {
        "A":120,
        "B":137,
        "N":0,
        "O":119
      },
      "ADDITIONAL_BILLABLE_NAMESPACES":[
        {
            "AKS_NAME": "run-stg-japaneast-aks",
            "NAMESPACE": "deleted-ns1",
            "invoice.param": "A-value",
            "contractid": "AG-990"
        }, 
        {
            "AKS_NAME": "run-stg-japaneast-aks",
            "NAMESPACE": "deleted-ns2",
            "invoice.param": "B-value",
            "contractid": "AG-992"
        }
    ],
      "config":{
        ...
      }
    }
    ```

    The `AZURE_AKS_LIST` field is an array of objects, where each object represents an Azure AKS cluster and contains the `AKS_NAME` property.

    The `VALUE_OF_BILLING` field is an object that maps billing parameters (identified by letters) to their corresponding values.
    The `ADDITIONAL_BILLABLE_NAMESPACES` is list of deleted namespaces which needs to be included for invoice generation.

    `Config` section includes various environment variable names, file naming conventions and configurations used by script.
      - `VAR_AZURE_TENANT_ID` : Azure tenant ID environment variable.
      - `VAR_AZURE_CLIENT_ID` : Azure client ID environment variable.
      - `VAR_AZURE_CLIENT_SECRET` : Azure client secret environment variable.
      - `VAR_SECURE_CLUSTER_CONFIG_FILE_LOCATION` : Location of the secure cluster environment file.
      - `VAR_AZURE_SUBSCRIPTION_ID` : Azure subscription ID environment variable.
      - `VAR_AKS_RESOURCE_GROUP` : Azure Kubernetes Service (AKS) resource group environment variable.
      - `CLUSTER_ROLE` : Role assigned to the cluster (e.g., "clusterUser").
      - `NODE_AZURE_AKS_LIST` : Node key name used in this file for AKS list- `AZURE_AKS_LIST`.
      - `NODE_VALUE_OF_BILLING` : Node key name associated with Value of billing- `VALUE_OF_BILLING`.
      - `NODE_AKS_NAME` : Name of the Azure Kubernetes Service (AKS) node.
      - `RAW_NAMESPACE_FILE_NAME` : Template for raw namespace JSON file names.
      - `NAMESPACE_FILE_NAME` : Template for namespace JSON file names.
      - `LABEL_VALUE_OF_BILLING` : Label name for the billing value parameter.
      - `LABEL_CONTRACT_ID` : Label name for the contract ID.
      - `ARTIFACTS_COMPILE_DATA_PATH` : Path for compiled data artifacts.
      - `ARTIFACTS_INVOICE_CSV_DATA_PATH` : Path for CSV invoice data artifacts.
      - `ARTIFACTS_RAW_DATA_PATH` : Path for raw data artifacts.
      - `VAR_ARIA_TO_TOKEN` : ARIA TO (Azure Resource Inventory & Analytics) token configuration variable.
      - `VAR_ARIA_MONTH_OFFSET` : ARIA TO month offset environment variable.
      - `VAR_ARIA_NAMESPACE_PREFIX` : ARIA TO namespace prefix environment variable.
      - `VAR_ARIA_API_URL` : ARIA TO API URL environment variable.
      - `ARIA_API_QUERY` : ARIA TO API query for resource quota calculation.
      - `RAW_ARIA_NAMESPACE_FILE_NAME` : Template for raw ARIA namespace JSON file names.
      - `DETAIL_ARIA_NAMESPACE_FILE_NAME` : Template for detailed ARIA namespace JSON file names.
      - `HEADER_ARIA_NAMESPACE_FILE_NAME` : Template for header ARIA namespace JSON file names.
      - `HEADER_ARIA_CONTRACT_FILE_NAME` : Template for contract header ARIA namespace JSON file names.
      - `DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME` : Template for detailed ARIA namespace CSV file names.
      - `HEADER_ARIA_NAMESPACE_CSV_FILE_NAME` : Template for header ARIA namespace CSV file names.
      - `HEADER_ARIA_CONTRACT_CSV_FILE_NAME` : Template for contract header ARIA namespace CSV file names.

### Conclusion
This README provides a detailed technical overview of the project, including the .gitlab-ci.yaml file, the AKSOperations.py script, and associated helper files. It explains the purpose of each file, the functionality they provide, and the dependencies required for the project to run successfully.s
