class LocalConfig:
    VAR_AZURE_TENANT_ID='e85feadf-11e7-47bb-a160-43b98dcc96f1'
    VAR_AZURE_CLIENT_ID='f84f60ba-c6a9-4eaa-9f33-95901ff99de5'
    VAR_AZURE_CLIENT_SECRET='Qq18Q~BUjDLm6ei2PL3VjkIXbNAOwm~DCkgFgbB9'
    VAR_SECURE_CLUSTER_CONFIG_FILE_LOCATION='pipeline_configuration.json'
    VAR_AZURE_SUBSCRIPTION_ID='90a13e75-e07a-41b0-9438-4c3950cc72d3'
    VAR_AKS_RESOURCE_GROUP='dscp-unit-test-dev-japaneast-rg'
    CLUSTER_ROLE='clusterUser'
    NODE_AZURE_AKS_LIST='AZURE_AKS_LIST'
    NODE_VALUE_OF_BILLING='VALUE_OF_BILLING'
    NODE_AKS_NAME='AKS_NAME'
    RAW_NAMESPACE_FILE_NAME='JsonFiles/RAW_AKS_{}_{}.json'#RAW_AKS_<ClusterName>_<YYYYMMDD>.json
    NAMESPACE_FILE_NAME='JsonFiles/AKS_{}.json'  
    LABEL_VALUE_OF_BILLING='invoice.param'
    LABEL_CONTRACT_ID='contractid'
    ARTIFACTS_COMPILE_DATA_PATH = "JsonFiles/"
    ARTIFACTS_RAW_DATA_PATH = "JsonFiles/"
    
class AriaToConfig:
    VAR_ARIA_TO_TOKEN='e21d2ee7-fc8b-4a30-866d-ff56cbc7acf4'
    VAR_ARIA_MONTH_OFFSET = 1
    VAR_ARIA_NAMESPACE_PREFIX ="stg-k8s-"
    VAR_ARIA_API_URL= 'https://sentosa.wavefront.com/api/v2/chart/api'
    ARIA_API_QUERY= 'if(ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="{}" and namespace_name="{}")))))/1024/1024/1024/5)>ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="{}" and namespace_name="{}")))))/1000),ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="{}" and namespace_name="{}")))))/1024/1024/1024/5),ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="{}" and namespace_name="{}")))))/1000))'
    RAW_ARIA_NAMESPACE_FILE_NAME='RAW_ARIA_{}_{}_{}.json' #RAW_ARIA_<ClusterName>_<NS>_<YYYYMMDD>.json
    DETAIL_ARIA_NAMESPACE_FILE_NAME='DETAIL_{}_{}_{}.json' #detail_<ClusterName>_<NS>_<YYYYMMDD>.json
    HEADER_ARIA_NAMESPACE_FILE_NAME='HEADER_{}.json' #HEADER_<YYYYMMM>.json
    DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME='DETAIL_{}_{}_{}.csv' #detail_<ClusterName>_<NS>_<YYYYMMDD>.json
    HEADER_ARIA_NAMESPACE_CSV_FILE_NAME='HEADER_{}.csv' #HEADER_<YYYYMMM>.json
