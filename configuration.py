class GitlabConfig:
    VAR_AZURE_TENANT_ID='AZURE_TENANT_ID'
    VAR_AZURE_CLIENT_ID='AZURE_CLIENT_ID'
    VAR_AZURE_CLIENT_SECRET='AZURE_CLIENT_SECRET'
    VAR_AZURE_SUBSCRIPTION_ID='AZURE_SUBSCRIPTION_ID'
    VAR_AKS_RESOURCE_GROUP='AKS_RESOURCE_GROUP'
    CLUSTER_ROLE='clusterUser'
    NODE_AZURE_AKS_LIST='AZURE_AKS_LIST'
    NODE_VALUE_OF_BILLING='VALUE_OF_BILLING'
    NODE_AKS_NAME='AKS_NAME'
    RAW_NAMESPACE_FILE_NAME='RAW_AKS_{}_{}.json'
    NAMESPACE_FILE_NAME='AKS_BILLABLE_NS_{}.json'
    LABEL_CONTRACT_ID='contractid'
    LABEL_VALUE_OF_BILLING='invoice.param'
    ARTIFACTS_COMPILE_DATA_PATH = "InvoiceGenerator/CompileData/"
    ARTIFACTS_INVOICE_CSV_DATA_PATH = "InvoiceGenerator/InvoiceCSV/"
    ARTIFACTS_RAW_DATA_PATH = "InvoiceGenerator/RawData/"


class AriaToConfig:
    VAR_ARIA_TO_TOKEN='ARIA_TO_TOKEN'
    VAR_ARIA_NAMESPACE_PREFIX='ARIA_NAMESPACE_PREFIX'
    VAR_ARIA_MONTH_OFFSET='ARIA_TO_MONTH_OFFSET'
    VAR_ARIA_API_URL= 'ARIA_API_URL'
    ARIA_API_QUERY= 'if(ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="{}" and namespace_name="{}")))))/1024/1024/1024/5)>ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="{}" and namespace_name="{}")))))/1000),ceil(align(1h,mean,rawsum(align(5m,mean,(ts("kubernetes.ns.memory.request", cluster="{}" and namespace_name="{}")))))/1024/1024/1024/5),ceil(align(1h,mean, rawsum(align(5m,mean,(ts("kubernetes.ns.cpu.request", cluster="{}" and namespace_name="{}")))))/1000))'
    RAW_ARIA_NAMESPACE_FILE_NAME='RAW_ARIA_{}_{}_{}.json' #RAW_ARIA_<ClusterName>_<NS>_<YYYYMMDD>.json
    DETAIL_ARIA_NAMESPACE_FILE_NAME='DETAIL_{}_{}_{}.json' #detail_<ClusterName>_<NS>_<YYYYMMDD>.json
    HEADER_ARIA_NAMESPACE_FILE_NAME='HEADER_{}.json' #HEADER_<YYYYMMM>.json
    DETAIL_ARIA_NAMESPACE_CSV_FILE_NAME='DETAIL_{}_{}_{}.csv' #detail_<ClusterName>_<NS>_<YYYYMMDD>.json
    HEADER_ARIA_NAMESPACE_CSV_FILE_NAME='HEADER_{}.csv' #HEADER_<YYYYMMM>.json 